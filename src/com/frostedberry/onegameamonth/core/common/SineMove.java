package com.frostedberry.onegameamonth.core.common;

public class SineMove {
	protected double length;
	
	protected double counter;
	protected double current;
	
	protected boolean completed;
	
	public SineMove(double length) {
		this.length = length;
	}
	
	public void next() {
		counter++;
		current = Math.sin((Math.PI / 2) * (counter / length)) * 100;
		if(current == 100) completed = true;
	}
	
	public boolean isCompleted() {
		return completed;
	}
	
	public double getCurrent() {
		return current;
	}
	
	public double getCurrentOne() {
		return current / 100.0;
	}
}
