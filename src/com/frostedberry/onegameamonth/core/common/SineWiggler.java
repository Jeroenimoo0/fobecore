package com.frostedberry.onegameamonth.core.common;

public class SineWiggler extends Wiggler {
	private double current;
	private int counter;
	private double speed;
	private double maximum;
	
	public SineWiggler(double maximum, double speed) {
		this.speed = speed;
		this.maximum = maximum;
	}
	
	public SineWiggler(double maximum, double speed, int start) {
		this.speed = speed;
		this.maximum = maximum;
		this.counter = start;
	}
	
	@Override
	public double getCurrent() {
		return current;
	}

	@Override
	public void next() {
		current = Math.sin(counter * speed) * maximum;
		counter++;
	}
}
