package com.frostedberry.onegameamonth.core.common;

public class Color {
	private float r;
	private float g;
	private float b;
	private float a;
	
	public Color() {
		r = 1;
		g = 1;
		b = 1;
		a = 1;
	}
	
	public Color(float r, float g, float b) {
		this.r = r;
		this.g = g;
		this.b = b;
		a = 1;
	}
	
	public Color(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public void fromRGB(float r, float g, float b) {
		this.r = r / 255;
		this.g = g / 255;
		this.b = b / 255;
	}

	public float getR() {
		return r;
	}

	public void setR(float r) {
		this.r = r;
	}

	public float getG() {
		return g;
	}

	public void setG(float g) {
		this.g = g;
	}

	public float getB() {
		return b;
	}

	public void setB(float b) {
		this.b = b;
	}

	public float getA() {
		return a;
	}

	public void setA(float a) {
		this.a = a;
	}
	
	
}
