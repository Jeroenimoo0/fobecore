package com.frostedberry.onegameamonth.core.common;

public abstract class Wiggler {
	abstract public double getCurrent();
	abstract public void next();
}
