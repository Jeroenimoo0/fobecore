package com.frostedberry.onegameamonth.core.data;

import java.util.HashMap;

public abstract class PropertyCollection {
	protected HashMap<String, Property> properties;
	protected boolean loaded;
	
	public PropertyCollection() {
		properties = new HashMap<>();
		init();
	}
	
	public void reset() {
		properties.clear();
		init();
	}
	
	abstract public void init();
	
	public void add(Property p) {
		properties.put(p.name, p);
	}
	
	public void loadFromDatabase() {
		for(Property p : properties.values()) {
			String value = Database.getProperty(p.name);
			if(value == null) p.value = p.defaultValue;
			else p.value = value;
		}
		
		loaded = true;
	}
	
	public void writeToDatabase() {
		Database.properties.clear();
		for(Property p : properties.values()) {
			Database.properties.setProperty(p.name, p.value);
		}
	}
	
	public void setProperty(String name, String value) {
		properties.get(name).value = value;
	}
	
	public void setProperty(String name, Object value) {
		properties.get(name).value = value.toString();
	}
	
	public String getProperty(String p) {
		return properties.get(p).value;
	}
	
	public HashMap<String, Property> getProperties() {
		return properties;
	}
	
	public boolean isLoaded() {
		return loaded;
	}
}
