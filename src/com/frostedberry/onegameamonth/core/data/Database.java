package com.frostedberry.onegameamonth.core.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import com.frostedberry.onegameamonth.core.Logger;

public class Database {
	protected static final String dir = System.getenv("APPDATA") + "/frostedberry/speedrun/";
	protected static String fileName = "speedrun.txt";
	
	protected static Properties properties;
	protected static boolean loaded;
	
	public static boolean loadDatabase() {
		File f = new File(dir + fileName);
		
		if(!f.exists()) createFile();
		
		properties = new Properties();
		try {
			properties.load(new FileInputStream(f));
			loaded = true;
			Logger.info("Succesfully loaded save file.");
			return true;
		} catch (IOException e) {
			Logger.info("Could not load properties file. well that's harsh :(");
			e.printStackTrace();
		}
		
		return false;
	}
	
	public static void createFile() {
		createPath();
		
		File f = new File(dir + fileName);
		try {
			f.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Logger.info("Succesfully created save file");
	}
	
	public static void createPath() {
		File f = new File(dir);
		if(!f.exists()) f.mkdirs();
	}
	
	public static String getProperty(String propertie) {
		return properties.getProperty(propertie);
	}
	
	public static void saveFile() {
		try {
			properties.store(new FileOutputStream(new File(dir + fileName)), "kaas");
			Logger.info("Succesfully saved file");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
