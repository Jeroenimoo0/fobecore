package com.frostedberry.onegameamonth.core.data;

public class Property {
	protected String name;
	protected String value;
	protected String defaultValue;
	
	public Property(String name, String defaulValue) {
		this.name = name;
		this.defaultValue = defaulValue;
		this.value = defaulValue;
	}
	
	public String getName() {
		return name;
	}
	
	public String getValue() {
		return value;
	}
	
	public String getDefaultValue() {
		return defaultValue;
	}
	
	public void setToDefault() {
		value = defaultValue;
	}
}
