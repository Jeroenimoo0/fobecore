package com.frostedberry.onegameamonth.core.texture;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import com.frostedberry.onegameamonth.core.Logger;

public 	class TextureLoadThread extends Thread {
	private TextureLoadList list;
	private Object resObject;
	
	public TextureLoadThread(TextureLoadList list, Object resObject) {
		this.list = list;
		this.resObject = resObject;
	}

	@Override
	public void start() {
		super.start();
		TextureLoader.done = false;
		TextureLoader.loading = true;
	}
	
	@Override
	public void run() {
		while(list.isNext()) {
			TextureLoadObject object = list.next();
			
			String filename = object.getFileName();
			BufferedImage image = TextureLoader.getImage(filename);
			if(image == null) {
				try {
					image = ImageIO.read(resObject.getClass().getResourceAsStream(filename + ".png"));
				} catch (Exception e) {
					Logger.error("Error while loading image: " + filename);
					e.printStackTrace();
				}
			}
			
			try {
				if(!object.isFullImage()) image = image.getSubimage(object.getX(), object.getY(), object.getWidth(), object.getHeight());
			} catch (Exception e) {
				Logger.error("Error while loading image: " + filename);
				e.printStackTrace();
			}
			
			TextureLoader.imagesToTexture.put(object.getSaveName(), image);
		}
		
		Logger.info("Done loading images");
		
		TextureLoader.loading = false;
		TextureLoader.done = true;
	}
}
