package com.frostedberry.onegameamonth.core.texture;

public class TextureLoadObject {
	private String filename;
	private String savename;
	private int x;
	private int y;
	private int w;
	private int h;
	private boolean full;
	
	public TextureLoadObject(String filename, String savename) {
		this.filename = filename;
		this.savename = savename;
		full = true;
	}
	
	public TextureLoadObject(String filename, String savename, int x, int y, int w, int h) {
		this.filename = filename;
		this.savename = savename;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		full = false;
	}
	
	public String getFileName() {
		return filename;
	}
	
	public String getSaveName() {
		return savename;
	}
	
	public int getWidth() {
		return w;
	}
	
	public int getHeight() {
		return h;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public boolean isFullImage() {
		return full;
	}
}
