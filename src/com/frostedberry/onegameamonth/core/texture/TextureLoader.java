package com.frostedberry.onegameamonth.core.texture;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.GL11;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.util.BufferedImageUtil;

import com.frostedberry.onegameamonth.core.Logger;

public class TextureLoader {
	protected static HashMap<String, Texture> textures = new HashMap<String, Texture>();
	protected static HashMap<String, BufferedImage> images = new HashMap<String, BufferedImage>();
	protected static HashMap<String, BufferedImage> imagesToTexture = new HashMap<String, BufferedImage>();
	
	protected static boolean loading;
	protected static boolean done;
	
	protected static Object resObject;
	
	public static void setResObject(Object resObject) {
		TextureLoader.resObject = resObject;
	}
	
	public static void loadTextures(TextureLoadList list, boolean threaded) {
		Logger.info("Started image loading...");
		if(threaded) new TextureLoadThread(list, resObject).start();
		else {
			while(list.isNext()) {
				TextureLoadObject object = list.next();
				
				String filename = object.getFileName();
				BufferedImage image = TextureLoader.getImage(filename);
				if(image == null) {
					try {
						image = ImageIO.read(resObject.getClass().getResourceAsStream(filename + ".png"));
					} catch (IOException e) {
						Logger.error("Error while loading image: " + filename);
						e.printStackTrace();
					}
				}
				
				if(!object.isFullImage()) image = image.getSubimage(object.getX(), object.getY(), object.getWidth(), object.getHeight());
				
				TextureLoader.imagesToTexture.put(object.getSaveName(), image);
			}
			
			Logger.info("Done loading images");
			Logger.info("Started texture loading...");
			
			for(String s : imagesToTexture.keySet()) {
				try {
					Texture texture = BufferedImageUtil.getTexture("PNG", imagesToTexture.get(s));
					textures.put(s, texture);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			Logger.info("Done loading textures");
		}
	}
	
	public static boolean checkForNewTextures() {
		if(done) {
			Logger.info("Started texture loading...");
			
			for(String s : imagesToTexture.keySet()) {
				try {
					Texture texture = BufferedImageUtil.getTexture("PNG", imagesToTexture.get(s));
					textures.put(s, texture);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			Logger.info("Done loading textures");
			return true;
		}
		
		return false;
	}
	
	public static Texture loadTexture(String filename) {
		BufferedImage image = TextureLoader.getImage(filename);
		if(image == null) {
			try {
				image = ImageIO.read(resObject.getClass().getResourceAsStream(filename + ".png"));
			} catch (IOException e) {
				Logger.error("Error while loading image: " + filename);
				e.printStackTrace();
			}
		}
		
		Texture texture = null;
		
		try {
			texture = BufferedImageUtil.getTexture("PNG", image);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return texture;
	}
	
	public static Texture loadTexture(String filename, String savename, int x, int y, int w, int h) {
		BufferedImage image = TextureLoader.getImage(filename);
		if(image == null) {
			try {
				image = ImageIO.read(resObject.getClass().getResourceAsStream(filename + ".png"));
				images.put(filename, image);
			} catch (IOException e) {
				Logger.error("Error while loading image: " + filename);
				e.printStackTrace();
			}
		}
		
		image = image.getSubimage(x, y, w, h);
		
		Texture texture = null;
		
		try {
			texture = BufferedImageUtil.getTexture("PNG", image);
			textures.put(savename, texture);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return texture;
	}
	
	public static Texture loadTexture(String filename, String savename) {
		BufferedImage image = TextureLoader.getImage(filename);
		if(image == null) {
			try {
				image = ImageIO.read(resObject.getClass().getResourceAsStream(filename + ".png"));
				images.put(filename, image);
			} catch (Exception e) {
				Logger.error("Error while loading image: " + filename);
				e.printStackTrace();
			}
		}
		
		Texture texture = null;
		
		try {
			texture = BufferedImageUtil.getTexture("PNG", image);
			textures.put(savename, texture);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return texture;
	}
	
	public static Texture getTexture(String name) {
		return textures.get(name);
	}
	
	public static BufferedImage getImage(String name) {
		return images.get(name);
	}
	
	public static boolean isLoading() {
		return loading;
	}
	
	public static boolean isDone() {
		return done;
	}
	
	public static void deleteTextures() {
		for(Texture t : textures.values()) {
			GL11.glDeleteTextures(t.getTextureID());
		}
		
		textures.clear();
		images.clear();
		imagesToTexture.clear();
		
		loading = false;
		done = false;
	}

	public static int size() {
		return textures.size();
	}
}
