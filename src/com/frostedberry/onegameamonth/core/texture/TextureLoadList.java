package com.frostedberry.onegameamonth.core.texture;

import java.util.ArrayList;
import java.util.List;

public class TextureLoadList {
	private List<TextureLoadObject> textureloadobjects = new ArrayList<TextureLoadObject>();
	
	public TextureLoadList() {
		
	}
	
	public TextureLoadList(TextureLoadObject[] tloa) {
		for(TextureLoadObject tlo : tloa) {
			textureloadobjects.add(tlo);
		}
	}
	
	private int c = 0;
	
	public void addTextureLoadObject(TextureLoadObject object) {
		textureloadobjects.add(object);
	}
	
	public boolean isNext() {
		return c < getSize();
	}
	
	public TextureLoadObject next() {
		if(c >= getSize()) reset();
		
		TextureLoadObject object = textureloadobjects.get(c);
		c++;
		return object;
	}
	
	public void reset() {
		c = 0;
	}
	
	public int getSize() {
		return textureloadobjects.size();
	}
	
	public void addToList(TextureLoadList tll) {
		for(TextureLoadObject tlo : textureloadobjects) {
			tll.addTextureLoadObject(tlo);
		}
	}
}
