package com.frostedberry.onegameamonth.core.tick;

import org.lwjgl.input.Keyboard;

public class KeyEvent {
	public int key;
	public boolean keyDown;
	public char eventChar;
	public long eventTime;
	
	public KeyEvent() {
		key = Keyboard.getEventKey();
		keyDown = Keyboard.getEventKeyState();
		eventTime = Keyboard.getEventNanoseconds();
		eventChar = Keyboard.getEventCharacter();
	}
}
