package com.frostedberry.onegameamonth.core.tick;

import org.lwjgl.input.Mouse;

public class MouseEvent {
	public int button;
	public boolean buttonDown;
	public int eventX;
	public int eventY;
	public int mouseDx;
	public int mouseDy;
	public int scroll;
	public long time;
	
	public MouseEvent() {
		button = Mouse.getEventButton();
		buttonDown = Mouse.getEventButtonState();
		eventX = Mouse.getEventX();
		eventY = Mouse.getEventY();
		mouseDx = Mouse.getEventDX();
		mouseDy = Mouse.getEventDY();
		scroll = Mouse.getEventDWheel();
		time = Mouse.getEventNanoseconds();
	}
}
