package com.frostedberry.onegameamonth.core.tick;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

public class Tick {
	protected List<KeyEvent> eventKeys;
	protected List<MouseEvent> eventMouse;
	
	public Tick() {
		eventKeys = new ArrayList<KeyEvent>();
		eventMouse = new ArrayList<MouseEvent>();
	}
	
	public void updateKeys() {
		eventKeys.clear();
		while(Keyboard.next()) {
			eventKeys.add(new KeyEvent());
		}
	}
	
	public void updateMouse() {
		eventMouse.clear();
		while(Mouse.next()) {
			eventMouse.add(new MouseEvent());
		}
	}
	
	public Tick fromTick(Tick tick) {
		eventKeys = tick.eventKeys;
		eventMouse = tick.eventMouse;
		return this;
	}

	public void update() {
		updateKeys();
		updateMouse();
	}
	
	public List<MouseEvent> getMouseEvents() {
		return eventMouse;
	}
	
	public List<KeyEvent> getKeyEvents() {
		return eventKeys;
	}
}
