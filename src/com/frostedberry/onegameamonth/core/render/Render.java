package com.frostedberry.onegameamonth.core.render;

import static org.lwjgl.opengl.GL11.*;

import com.frostedberry.onegameamonth.core.Logger;
import com.frostedberry.onegameamonth.core.common.Color;
import com.frostedberry.onegameamonth.core.texture.TextureLoader;

public class Render {
	public static void renderTexture(String textureName, float x, float y, float w, float h, float r, float g, float b) {
		renderTexture(textureName, x, y, 0, w, h, r, g, b);
	}
	
	public static void renderTexture(String textureName, float x, float y, float w, float h, Color color) {
		renderTexture(textureName, x, y, 0, w, h, color.getR(), color.getG(), color.getB());
	}
	
	public static void renderTexture(String textureName, float x, float y, float w, float h) {
		renderTexture(textureName, x, y, 0, w, h, 1, 1, 1);
	}
	
	public static void renderTexture(String textureName, float x, float y, float s) {
		renderTexture(textureName, x, y, 0, s, s, 1, 1, 1);
	}
	
	public static void renderTexture(String textureName, float x, float y, float z, float w, float h, float r, float g, float b) {
		renderTexture(textureName, x, y, z, w, h, r, g, b, 1);
	}
	
	public static void renderTexture(String textureName, float x, float y, float z, float w, float h, float r, float g, float b, float a) {
		glEnable(GL_TEXTURE_2D);
		
		glPushMatrix();
		
		if(TextureLoader.getTexture(textureName) == null) Logger.info(textureName);
		TextureLoader.getTexture(textureName).bind();
		glColor4f(r, g, b, a);
		
		glTranslatef(x, y, 0);
		
		glBegin(GL_QUADS);
		
		glTexCoord2f(0, 0);
		glVertex2f(-w, -h);
		
		glTexCoord2f(1, 0);
		glVertex2f(+w, -h);
		
		glTexCoord2f(1, 1);
		glVertex2f(+w, +h);
		
		glTexCoord2f(0, 1);
		glVertex2f(-w, +h);
		
		glEnd();
		
		glPopMatrix();
		
		glDisable(GL_TEXTURE_2D);
	}

	public static void renderTexturePixel(String textureName, float x, float y, float w, float h, float r, float g, float b, float a) {
		renderTexturePixel(textureName, x, y, 0, w, h, r, g, b, a);
	}
	
	public static void renderTexturePixel(String textureName, float x, float y, float w, float h, float r, float g, float b) {
		renderTexturePixel(textureName, x, y, 0, w, h, r, g, b, 1);
	}
	
	public static void renderTexturePixel(String textureName, float x, float y, float w, float h, Color color) {
		renderTexturePixel(textureName, x, y, 0, w, h, color.getR(), color.getG(), color.getB(), 1);
	}
	
	public static void renderTexturePixel(String textureName, float x, float y, float w, float h) {
		renderTexturePixel(textureName, x, y, 0, w, h, 1, 1, 1, 1);
	}
	
	public static void renderTexturePixel(String textureName, float x, float y, float s) {
		renderTexturePixel(textureName, x, y, 0, s, s, 1, 1, 1, 1);
	}
	
	public static void renderTexturePixel(String textureName, float x, float y, float z, float w, float h, float r, float g, float b, float a) {
		glEnable(GL_TEXTURE_2D);
		
		glPushMatrix();
		
		if(TextureLoader.getTexture(textureName) == null) Logger.info("Texture: " + textureName + " is null");
		TextureLoader.getTexture(textureName).bind();
		glColor4f(r, g, b, a);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
		glTranslatef(x, y, 0);
		
		glBegin(GL_QUADS);
		
		glTexCoord2f(0, 0);
		glVertex2f(-w, -h);
		
		glTexCoord2f(1, 0);
		glVertex2f(+w, -h);
		
		glTexCoord2f(1, 1);
		glVertex2f(+w, +h);
		
		glTexCoord2f(0, 1);
		glVertex2f(-w, +h);
		
		glEnd();
		
		glPopMatrix();
		
		glDisable(GL_TEXTURE_2D);
	}
	
	public static void renderColor(float x, float y, float w, float h, float r, float g, float b) {
		renderColor(x, y, 0, w, h, r, g, b);
	}
	
	public static void renderColor(float x, float y, float w, float h, Color color) {
		renderColor(x, y, 0, w, h, color.getR(), color.getG(), color.getB());
	}
	
	public static void renderColor(float x, float y, float w, float h) {
		renderColor(x, y, 0, w, h, 1, 1, 1);
	}
	
	public static void renderColor(float x, float y, float s) {
		renderColor(x, y, 0, s, s, 1, 1, 1);
	}
	
	public static void renderColor(float x, float y, float z, float w, float h, Color color) {
		renderColor(x, y, z, w, h, color.getR(), color.getG(), color.getB());
	}
	
	public static void renderColor(float x, float y, float z, float w, float h, float r, float g, float b) {
		renderColor(x, y, z, w, h, r, g, b, 1);
	}
	
	public static void renderColor(float x, float y, float z, float w, float h, float[] r, float[] g, float[] b, float[] a) {
		glPushMatrix();
		
		glTranslatef(x, y, 0);
		
		glBegin(GL_QUADS);
		
		glColor4f(r[0], g[0], b[0], a[0]);
		glVertex2f(-w, -h);
		
		glColor4f(r[1], g[1], b[1], a[1]);
		glVertex2f(+w, -h);
		
		glColor4f(r[2], g[2], b[2], a[2]);
		glVertex2f(+w, +h);
		
		glColor4f(r[3], g[3], b[3], a[3]);
		glVertex2f(-w, +h);
		
		glEnd();
		
		glPopMatrix();
	}
	
	public static void renderColor(float x, float y, float z, float w, float h, float r, float g, float b, float a) {
		glPushMatrix();
		
		glColor4f(r, g, b, a);
		
		glTranslatef(x, y, 0);
		
		glBegin(GL_QUADS);
		
		glVertex2f(-w, -h);
		
		glVertex2f(+w, -h);
		
		glVertex2f(+w, +h);
		
		glVertex2f(-w, +h);
		
		glEnd();
		
		glPopMatrix();
	}
	
	public static void renderString(String s, float x, float y, float size) {
		renderString(s, x, y, size, 1, 1, 1, 1);
	}
	
	public static void renderString(String s, float x, float y, float size, float r, float g, float b) {
		renderString(s, x, y, size, r, g, b, 1);
	}
	
	public static void renderString(String s, float x, float y, float size, float r, float g, float b, float a) {
		float xo = 0;
		for(int c = 0; c < s.length(); c++) {
			if(s.charAt(c) != " ".charAt(0)) {
				renderChar(s.charAt(c), xo * 0.8f * size + x, y - size * getCharHeightOffset(s.charAt(c)), size, r, g, b, a);
			}
			
			xo += getCharOffset(s.charAt(c));
		}
	}
	
	public static void renderStringCentered(String s, float x, float y, float size, float r, float g, float b) {
		renderStringCentered(s, x, y, size, r, g, b, 1);
	}
	
	public static void renderStringCentered(String s, float x, float y, float size, float r, float g, float b, float a) {
		float length = 0;
		for(int c = 0; c < s.length(); c++) {
			length += getCharOffset(s.charAt(c));
		}
		
		float offset = -length / 2;
		
		for(int c = 0; c < s.length(); c++) {
			if(s.charAt(c) != " ".charAt(0)) {
				renderChar(s.charAt(c), offset * 0.8f * size + x, y - size * getCharHeightOffset(s.charAt(c)), size, r, g, b, a);
			}
			
			offset += getCharOffset(s.charAt(c));
		}
	}
	
	private static float getCharOffset(char c) {
		if(c == 'i' || c == 'l' || c == 'I') {
			return 0.5f;
		}
		else if(c == 'k' || c == '\'') {
			return 0.8f;
		}
		else if(c == 't') {
			return 0.65f;
		}
		else return 1;
	}
	
	private static float getCharHeightOffset(char c) {
		if(c == 'J' || c == 'C' || c == 'H' || c == 'I' || c == 'T' || c == 'S' || c == '.'
		|| c == 'F' || c == 'R' || c == 'Q' || c == 'A' || c == 'O' || c == 'G' || c == 'M'
		|| c == 'D' || c == 'V' || c == 'E' || c == 'N' || c == '!' || c == 'L' || c == '1'
		|| c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8'
		|| c == '9' || c == '0' || c == 'B' ) {
			return 0.1f;
		}
		else if(c == 'W') {
			return 0.12f;
		}
		else if(c == 'p') {
			return -0.1f;
		}
		else return 0;
	}
	
	public static void renderChar(String s, float x, float y, float size) {
		if(s.length() == 1) renderChar(s.toCharArray()[0], x, y, size);
	}
	
	public static void renderChar(String s, float x, float y, float size, float r, float g, float b) {
		if(s.length() == 1) renderChar(s.toCharArray()[0], x, y, size, r, g, b, 1);
	}
	
	public static void renderChar(char c, float x, float y, float size) {
		renderChar(c, x, y, size, 1, 1, 1, 1);
	}
	
	public static void renderChar(char c, float x, float y, float size, float r, float g, float b, float a) {
		glEnable(GL_TEXTURE_2D);
		TextureLoader.getTexture("font_" + c).bind();
		if(size >= 0) glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		else glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glColor4f(r, g, b, a);
		
		glBegin(GL_QUADS);
		
		glTexCoord2f(0, 0);
		glVertex2f(x, y);
		
		glTexCoord2f(1, 0);
		glVertex2f(x + size, y);
		
		glTexCoord2f(1, 1);
		glVertex2f(x + size, y + size);
		
		glTexCoord2f(0, 1);
		glVertex2f(x, y + size);
		
		glEnd();
		
		glDisable(GL_TEXTURE_2D);
	}
	
	public static void renderLine(float x, float y, float x2, float y2) {
		renderLine(x, y, x2, y2, 1);
	}
	
	public static void renderLine(float x, float y, float x2, float y2, float w) {
		renderLine(x, y, x2, y2, w, 1, 1, 1);
	}
	
	public static void renderLine(float x, float y, float x2, float y2, float w, Color color) {
		renderLine(x, y, x2, y2, w, color.getR(), color.getG(), color.getB());
	}
	
	public static void renderLine(float x, float y, float x2, float y2, float w, float r, float g, float b) {
		glColor3f(r, g, b);
		
		glLineWidth(w);
		
		glBegin(GL_LINES);
		
		glVertex2f(x, y);
		
		glVertex2f(x2, y2);
		
		glEnd();
	}
}
