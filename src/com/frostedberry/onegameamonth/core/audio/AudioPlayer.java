package com.frostedberry.onegameamonth.core.audio;

import java.util.HashMap;

import org.lwjgl.openal.AL10;

public class AudioPlayer {
	protected static HashMap<String, Integer> sources = new HashMap<>();
	protected static float volume = 1f;
	
	public static void playAsMusic(String name, float f1, float f2, boolean loop) {
		int id = AudioLoader.getAudio(name).playAsMusic(f1, f2, loop);
		AL10.alSourcef(id, AL10.AL_GAIN, volume);
		sources.put(name, id);
	}
	
	public static void playAsSound(String name, float f1, float f2, boolean loop) {
		int id = AudioLoader.getAudio(name).playAsSoundEffect(f1, f2, loop);
		AL10.alSourcef(id, AL10.AL_GAIN, volume);
		sources.put(name, id);
	}
	
	public static void setVolume(float volume) {
		AudioPlayer.volume = volume;
		for(int i : sources.values()) {
			AL10.alSourcef(i, AL10.AL_GAIN, volume);
		}
	}
	
	public static float getVolume() {
		return volume;
	}
	
	public static void changeVolume(String name, float volume) {
		if(sources.get(name) != null) {
			AL10.alSourcef(sources.get(name), AL10.AL_GAIN, volume);
		}
	}
	
	public static float getAudioVolume(String name) {
		return AL10.alGetSourcef(sources.get(name), AL10.AL_GAIN);
	}

	public static void stop(String name) {
		AudioLoader.getAudio(name).stop();
	}

	public static void stopAllAudio() {
		for(String s : sources.keySet()) {
			AudioLoader.getAudio(s).stop();
		}
	}
}
