package com.frostedberry.onegameamonth.core.audio;

import java.util.ArrayList;
import java.util.List;

public class AudioLoadList {
	private List<AudioLoadObject> audioLoadObjects = new ArrayList<>();
	
	public AudioLoadList(List<AudioLoadObject> soundLoadObjects) {
		this.setAudioLoadObjects(soundLoadObjects);
	}
	
	public AudioLoadList() {
		
	}

	public List<AudioLoadObject> getAudioLoadObjects() {
		return audioLoadObjects;
	}

	public void setAudioLoadObjects(List<AudioLoadObject> audioLoadObjects) {
		this.audioLoadObjects = audioLoadObjects;
	}
	
	public int getSize() {
		return audioLoadObjects.size();
	}
	
	public void add(AudioLoadObject alo) {
		audioLoadObjects.add(alo);
	}
}
