package com.frostedberry.onegameamonth.core.audio;

import java.io.InputStream;
import java.util.HashMap;

import org.newdawn.slick.openal.Audio;

import com.frostedberry.onegameamonth.core.Logger;

public class AudioLoader {
	protected static HashMap<String, Audio> audio = new HashMap<String, Audio>();
	
	protected static boolean loading;
	protected static boolean done;
	
	protected static Object resObject;
	
	public static void setResObject(Object resObject) {
		AudioLoader.resObject = resObject;
	}
	
	public static void loadSounds(AudioLoadList sll) {
		long ms = System.currentTimeMillis();
		for(AudioLoadObject alo : sll.getAudioLoadObjects()) {
			try {
				InputStream i = resObject.getClass().getResourceAsStream(alo.getFilename() + ".ogg");
				Audio a = org.newdawn.slick.openal.AudioLoader.getAudio("OGG", i);
				audio.put(alo.getSavename(), a);
			} catch (Exception e) {
				Logger.info("Error while loading " + alo.getFilename() + ".ogg");
				e.printStackTrace();
			}
		}
		
		Logger.info("succesfully loaded " + sll.getSize() + " sound in " + (System.currentTimeMillis() - ms) + "ms.");
	}
	
	public static Audio getAudio(String name) {
		return audio.get(name);
	}
}
