package com.frostedberry.onegameamonth.core.audio;

public class AudioLoadObject {
	private String filename;
	private String savename;
	
	public AudioLoadObject(String filename, String savename) {
		this.setFilename(filename);
		this.setSavename(savename);
	}

	public String getSavename() {
		return savename;
	}

	public void setSavename(String savename) {
		this.savename = savename;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
