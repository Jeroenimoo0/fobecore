package com.frostedberry.onegameamonth.core.text;

import com.frostedberry.onegameamonth.core.texture.TextureLoadList;
import com.frostedberry.onegameamonth.core.texture.TextureLoadObject;
import com.frostedberry.onegameamonth.core.texture.TextureLoader;

public class TextLoader {
	private static boolean loaded;
	
	public static void loadAllText() {
		TextureLoader.loadTextures(getTextureLoadList(), false);
		loaded = true;
	}
	
	public static TextureLoadList getTextureLoadList() {
		TextureLoadList list = new TextureLoadList();
		
		list.addTextureLoadObject(new TextureLoadObject("font", "font_0", 0, 0, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_1", 8, 0, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_2", 16, 0, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_3", 24, 0, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_4", 32, 0, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_5", 40, 0, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_6", 48, 0, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_7", 56, 0, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_8", 64, 0, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_9", 72, 0, 8, 8));
		
		list.addTextureLoadObject(new TextureLoadObject("font", "font_A", 0, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_B", 8, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_C", 16, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_D", 24, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_E", 32, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_F", 40, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_G", 48, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_H", 56, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_I", 64, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_J", 72, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_K", 80, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_L", 88, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_M", 96, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_N", 104, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_O", 112, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_P", 120, 8, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_Q", 0, 16, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_R", 8, 16, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_S", 16, 16, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_T", 24, 16, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_U", 32, 16, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_V", 40, 16, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_W", 48, 16, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_X", 56, 16, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_Y", 64, 16, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_Z", 72, 16, 8, 8));
		
		list.addTextureLoadObject(new TextureLoadObject("font", "font_a", 0, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_b", 8, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_c", 16, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_d", 24, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_e", 32, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_f", 40, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_g", 48, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_h", 56, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_i", 64, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_j", 72, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_k", 80, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_l", 88, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_m", 96, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_n", 104, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_o", 112, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_p", 120, 24, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_q", 0, 32, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_r", 8, 32, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_s", 16, 32, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_t", 24, 32, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_u", 32, 32, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_v", 40, 32, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_w", 48, 32, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_x", 56, 32, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_y", 64, 32, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_z", 72, 32, 8, 8));
		
		list.addTextureLoadObject(new TextureLoadObject("font", "font_!", 0, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_@", 8, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_#", 16, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_$", 24, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_%", 32, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_^", 40, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_&", 48, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_*", 56, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_(", 64, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_)", 72, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font__", 80, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_+", 88, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_-", 96, 40, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_=", 104, 40, 8, 8));
		
		list.addTextureLoadObject(new TextureLoadObject("font", "font_{", 0, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_}", 8, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_[", 16, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_]", 24, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_:", 32, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_;", 40, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_\"", 48, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_'", 56, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_|", 64, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_\\", 72, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_<", 80, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_>", 88, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_,", 96, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_.", 104, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_?", 112, 48, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_/", 120, 48, 8, 8));
		
		list.addTextureLoadObject(new TextureLoadObject("font", "font_`", 0, 56, 8, 8));
		list.addTextureLoadObject(new TextureLoadObject("font", "font_~", 0, 56, 8, 8));
		
		return list;
	}
	
	public static boolean isLoaded() {
		return loaded;
	}
}
