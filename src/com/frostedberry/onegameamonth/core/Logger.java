package com.frostedberry.onegameamonth.core;

public class Logger {
	private static Core core;
	
	public static void info(String s) {
		if(core != null) System.out.println("[" + core.getGameName() + "] " + s);
		else System.out.println("[Unknown] " + s);
	}
	
	public static void error(String s) {
		if(core != null) System.out.println("[" + core.getGameName() + "] [ERROR]" + s);
		else System.out.println("[Unknown] " + s);
	}
	
	public static void setCore(Core core) {
		Logger.core = core;
	}
}
