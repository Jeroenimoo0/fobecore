package com.frostedberry.onegameamonth.core.gui;

import org.lwjgl.input.Keyboard;

import com.frostedberry.onegameamonth.core.Core;
import com.frostedberry.onegameamonth.core.Logger;
import com.frostedberry.onegameamonth.core.audio.AudioLoadList;
import com.frostedberry.onegameamonth.core.audio.AudioLoader;
import com.frostedberry.onegameamonth.core.data.Database;
import com.frostedberry.onegameamonth.core.gui.Gui;
import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.text.TextLoader;
import com.frostedberry.onegameamonth.core.texture.TextureLoadList;
import com.frostedberry.onegameamonth.core.texture.TextureLoader;
import com.frostedberry.onegameamonth.core.tick.KeyEvent;
import com.frostedberry.onegameamonth.core.tick.Tick;

public abstract class GuiLoadScreenTemplate extends Gui {
	protected boolean startedLoading;
	protected boolean doneLoading;
	
	protected int ticks;
	protected int loadTextureTicks;
	protected int loadTexture;
	protected boolean loadTextureDone;
	
	protected long startTime;
	
	protected boolean autodone = core.isDebug();
	
	public GuiLoadScreenTemplate(Core core) {
		super("GuiLoadScreen", core);
		
		TextureLoader.deleteTextures();
		
		TextureLoader.loadTexture("logo", "logo");
		
		TextureLoader.loadTexture("loading", "loading_0", 0, 0, 32, 32);
		TextureLoader.loadTexture("loading", "loading_1", 32, 0, 32, 32);
		TextureLoader.loadTexture("loading", "loading_2", 64, 0, 32, 32);
		TextureLoader.loadTexture("loading", "loading_3", 96, 0, 32, 32);
		
		TextureLoader.loadTexture("loading", "loading_4", 0, 32, 32, 32);
		TextureLoader.loadTexture("loading", "loading_5", 32, 32, 32, 32);
		TextureLoader.loadTexture("loading", "loading_6", 64, 32, 32, 32);
		TextureLoader.loadTexture("loading", "loading_7", 96, 32, 32, 32);
		
		TextureLoader.loadTexture("loading", "loading_8", 0, 64, 32, 32);
		TextureLoader.loadTexture("loading", "loading_9", 32, 64, 32, 32);
		TextureLoader.loadTexture("loading", "loading_10", 64, 64, 32, 32);
		TextureLoader.loadTexture("loading", "loading_11", 96, 64, 32, 32);
		
		TextureLoader.loadTexture("loading", "loading_12", 0, 96, 32, 32);
		TextureLoader.loadTexture("loading", "loading_13", 32, 96, 32, 32);
		TextureLoader.loadTexture("loading", "loading_14", 64, 96, 32, 32);
		TextureLoader.loadTexture("loading", "loading_15", 96, 96, 32, 32);
		
		TextLoader.loadAllText();
	}

	@Override
	public void render() {		
		float x = core.getGameWidth() / 2;
		float y = core.getGameHeight() / 2;
		
		Render.renderTexture("logo", x, y, 48, 48, 1, 1, 1);
		
		if(!loadTextureDone) {
			x = core.getGameWidth() - 20;
			y = 20;
			
			Render.renderTexture("loading_" + loadTexture, x, y, 8, 8, .3f, .5f, 1f);
		}
		else {
			if(!autodone) {
				Render.renderString("Press any key", 4, 180 - 8, 6, .3f, .5f, 1f);
			}
		}
		
		if(!core.isFullscreen()) Render.renderStringCentered("Fullscreen [F11]", 160, 2, 8, 1, 1, 1, 1);
	}

	@Override
	public void tick(Tick tick) {
		ticks++;
		
		if(!loadTextureDone) {
			if(!doneLoading || ticks < 0) {
				loadTextureTicks++;
				if(loadTextureTicks >= 5) {
					loadTextureTicks = 0;
					if(loadTexture < 6) loadTexture++;
					else loadTexture = 0;
				}
			}
			else {
				loadTextureTicks++;
				if(loadTextureTicks >= 3) {
					loadTextureTicks = 0;
					if(loadTexture < 14) loadTexture++;
					else loadTextureDone = true;
				}
			}
		}
		
		if(!doneLoading) {
			if(!TextureLoader.isLoading() && !TextureLoader.isDone()) {
				startTime = System.currentTimeMillis();
				
				TextureLoader.loadTextures(getTextureLoadList(), true);
			}
			
			if(TextureLoader.checkForNewTextures()) {
				doneLoading = true;
				Logger.info("Done loading textures in " + (System.currentTimeMillis() - startTime) + "ms");
				AudioLoader.loadSounds(getAudioLoadList());
				
				Database.loadDatabase();
				core.getPropertyCollection().loadFromDatabase();
				core.onPropertiesLoaded();
			}
		}
		
		boolean keyPressed = false;
		
		for(KeyEvent ke : tick.getKeyEvents()) {
			if(ke.key != Keyboard.KEY_F11 && ke.key != Keyboard.KEY_P) keyPressed = true;
		}
		
		if((keyPressed || autodone) && loadTextureDone && doneLoading ) {
			done = true;
		}
	}
	
	abstract protected TextureLoadList getTextureLoadList();
	abstract protected AudioLoadList getAudioLoadList();
}
