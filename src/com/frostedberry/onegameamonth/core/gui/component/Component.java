package com.frostedberry.onegameamonth.core.gui.component;

import com.frostedberry.onegameamonth.core.tick.Tick;

public abstract class Component {
	public abstract void render();
	public abstract void tick(Tick tick);
}
