package com.frostedberry.onegameamonth.core.gui.component;

import com.frostedberry.onegameamonth.core.render.Render;
import com.frostedberry.onegameamonth.core.tick.Tick;

public class Button extends Component {
	protected String text;
	protected float x;
	protected float y;
	protected float w;
	protected float h;
	
	public Button(float x, float y) {
		this.x = x;
		this.y = y;
		text = "null";
	}
	
	public Button(float x, float y, String text) {
		this.x = x;
		this.y = y;
		this.text = text;
	}
	
	public Button(float x, float y, float w, float h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		text = "null";
	}
	
	public Button(float x, float y, float w, float h, String text) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.text = text;
	}
	
	@Override
	public void render() {
		Render.renderColor(x, y, 0, w, h, 1, 1, 1, 1);
	}

	@Override
	public void tick(Tick tick) {
		
	}
}
