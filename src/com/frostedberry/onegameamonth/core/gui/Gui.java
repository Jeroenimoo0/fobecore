package com.frostedberry.onegameamonth.core.gui;

import java.util.ArrayList;
import java.util.List;

import com.frostedberry.onegameamonth.core.Core;
import com.frostedberry.onegameamonth.core.gui.component.Component;
import com.frostedberry.onegameamonth.core.tick.Tick;

public abstract class Gui {
	protected String name;
	protected boolean done;
	public Core core;
	protected List<Component> components;
	
	public Gui(String name, Core core) {
		this.name = name;
		this.core = core;
		components = new ArrayList<>();
	}
	
	abstract public Gui getNextGui();
	abstract public void render();
	abstract public void tick(Tick tick);
	
	public void renderComponents() {
		for(Component c : components) {
			c.render();
		}
	}
	
	protected void tickComponents(Tick tick) {
		for(Component c : components) {
			c.tick(tick);
		}
	}
	
	public boolean isDone() {
		return done;
	}
	
	public void setGui(Gui gui) {
		core.setGui(gui);
	}
}
