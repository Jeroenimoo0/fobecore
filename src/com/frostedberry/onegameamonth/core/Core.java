package com.frostedberry.onegameamonth.core;

import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.glClear;
import static org.lwjgl.opengl.GL11.glViewport;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Canvas;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.openal.AL;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.openal.SoundStore;

import com.frostedberry.onegameamonth.core.audio.AudioLoader;
import com.frostedberry.onegameamonth.core.data.PropertyCollection;
import com.frostedberry.onegameamonth.core.gui.Gui;
import com.frostedberry.onegameamonth.core.gui.GuiLoadScreenTemplate;
import com.frostedberry.onegameamonth.core.texture.TextureLoader;
import com.frostedberry.onegameamonth.core.tick.Tick;

public abstract class Core extends Applet {
	/**
	 * @author Jeroen van de Haterd aka. Jeroenimoo0
	 * Copyright Frostedberry
	 */
	private static final long serialVersionUID = 1L;
	
	private long lastPrint;
	private int ticks;
	private int frames;
	private Tick tick;
	private int fps;
	private int tps;
	
	private boolean fullscreen;
	
	private Canvas display_parent;
	private Thread gameThread;
	private boolean running = false;
	private boolean loopRunning = false;
	
	private boolean printFps = false;
	private boolean showFps = false;
	
	private boolean inBrowser;
	
	private Gui gui;
	
	public void startLWJGL() {
		gameThread = new Thread() {
			public void run() {
				running = true;
				try {
					Display.setParent(display_parent);
					Display.create();
					
					//Display.setDisplayConfiguration(1, -1f, 1);
					
					initBeforeGl();
					
					tick = new Tick();
					TextureLoader.setResObject(getResObject());
					AudioLoader.setResObject(getResObject());
					
					initGl();
					initAfterGl();
				} catch (LWJGLException e) {
					e.printStackTrace();
					return;
				}
				
				gameLoop();
			}
		};
		gameThread.start();
	}
	
	private void stopLWJGL() {
		running = false;
		try {
			gameThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void gameLoop() {
		gui = getLoadScreen();
		
		long waitTime = 0;
		long last = 0;
		
		loopRunning = true;
		while(running) {
			if(Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)  && Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) && isDebug()) gui = getLoadScreen();
			if(Keyboard.isKeyDown(Keyboard.KEY_F4) && !isInBrowser()) running = false;
			
			if(System.nanoTime() - last >= waitTime) {
				long start = System.nanoTime();
				
				tick.update();
				mainTick();
				ticks++;
				
				mainRender();
				frames++;
				
				long timeTaken = System.nanoTime() - start;
				
				//System.out.println(1_000_000_000 - timeTaken * 60);
				
				waitTime = (1_000_000_000 - timeTaken * 60 * (long)1.25) / 60;
				last = System.nanoTime();
			}
			
			/*
			if(System.nanoTime() - lastRender >= 16444444 && frames < 60) {
				mainRender();
				frames++;
				lastRender = System.nanoTime();
			}
			
			if(System.nanoTime() - lastTick >= 16444444 && ticks < 60) {
				tick.update();
				mainTick();
				ticks++;
				lastTick = System.nanoTime();
			}
			*/
			
			if(System.currentTimeMillis() - lastPrint > 1000) {
				if(isPrintFps()) System.out.println("Current fps: " + frames + ". ticks: " + ticks);
				fps = frames;
				tps = ticks;
				ticks = 0;
				frames = 0;
				lastPrint = System.currentTimeMillis();
			}
		}
		
		Logger.info("Stopping...");
		
		AL.destroy();
		Display.destroy();
		
		//stop();
		//destroy();
	}
	
	private void mainRender() {
		glClear(GL_COLOR_BUFFER_BIT);
		
		if(getGui() != null) getGui().render();
		
		render();
		
		Display.update();
	}
	
	private void mainTick() {
		tick(tick);
		if(gui != null) {
			if(gui.isDone()) gui = gui.getNextGui();
			gui.tick(tick);
		}
		SoundStore.get().poll(0);
	}
	
	protected abstract void render();
	protected abstract void tick(Tick tick);
	protected abstract void initBeforeGl();
	protected abstract void initGl();
	protected abstract void initAfterGl();
	protected abstract GuiLoadScreenTemplate getLoadScreen();
	public abstract void start();
	public abstract void stop();
	public abstract Object getResObject();
	public abstract int getGameWidth();
	public abstract int getGameHeight();
	public abstract boolean isDebug();
	public abstract int getWindowWidth();
	public abstract int getWindowHeight();
	public abstract PropertyCollection getPropertyCollection();
	public abstract void onPropertiesLoaded();
	public abstract String getGameName();
	
	public void init() {
		Logger.setCore(this);
		setLayout(new BorderLayout());
		try {
			display_parent = new Canvas() {
				private static final long serialVersionUID = 1L;
				public final void addNotify() {
					super.addNotify();
					startLWJGL();
				}
				public final void removeNotify() {
					stopLWJGL();
					super.removeNotify();
				}
			};
			display_parent.setSize(getWidth(),getHeight());
			add(display_parent);
			display_parent.setFocusable(true);
			display_parent.requestFocus();
			display_parent.setIgnoreRepaint(true);
			
			String browser = getParameter("browser");
			if(browser != null) {
				try {
					inBrowser = Boolean.parseBoolean(browser);
				}
				catch(Exception e) {
					e.printStackTrace();
					inBrowser = false;
				}
			}
			
			if(!inBrowser) {
				Logger.info("Not running in browser.");
				setSize(getWindowWidth(), getWindowHeight());
			}
			else {
				Logger.info("Running in browser.");
			}
			setVisible(true);
		} catch (Exception e) {
			System.err.println(e);
			throw new RuntimeException("Unable to create display");
		}
	}
	
	public void destroy() {
		remove(display_parent);
		super.destroy();
		Logger.info("Stopped.");
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public boolean isLoopRunning() {
		return loopRunning;
	}
	
	public boolean isInBrowser() {
		return inBrowser;
	}

	public Gui getGui() {
		return gui;
	}

	public void setGui(Gui gui) {
		this.gui = gui;
	}
	
	public int getFps() {
		return fps;
	}
	
	public int getTps() {
		return tps;
	}

	public boolean isPrintFps() {
		return printFps;
	}

	public void setPrintFps(boolean printFps) {
		this.printFps = printFps;
	}

	public boolean isShowFps() {
		return showFps;
	}

	public void setShowFps(boolean showFps) {
		this.showFps = showFps;
	}
	
	public void enableFullScreen() {
		try {
			Display.setFullscreen(true);
			glViewport(0, 0, Display.getWidth(), Display.getHeight());
			fullscreen = true;
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	public void disableFullScreen() {
		try {
			Display.setFullscreen(false);
			glViewport(0, 0, Display.getWidth(), Display.getHeight());
			fullscreen = false;
		} catch (LWJGLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Set the display mode to be used 
	 * 
	 * @param width The width of the display required
	 * @param height The height of the display required
	 * @param fullscreen True if we want fullscreen mode
	 */
	public void setDisplayMode(int width, int height, boolean fullscreen) {

	    // return if requested DisplayMode is already set
	    if ((Display.getDisplayMode().getWidth() == width) && 
	        (Display.getDisplayMode().getHeight() == height) && 
		(Display.isFullscreen() == fullscreen)) {
		    return;
	    }

	    try {
	        DisplayMode targetDisplayMode = null;
			
		if (fullscreen) {
		    DisplayMode[] modes = Display.getAvailableDisplayModes();
		    int freq = 0;
					
		    for (int i=0;i<modes.length;i++) {
		        DisplayMode current = modes[i];
						
			if ((current.getWidth() == width) && (current.getHeight() == height)) {
			    if ((targetDisplayMode == null) || (current.getFrequency() >= freq)) {
			        if ((targetDisplayMode == null) || (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel())) {
				    targetDisplayMode = current;
				    freq = targetDisplayMode.getFrequency();
	                        }
	                    }

			    // if we've found a match for bpp and frequence against the 
			    // original display mode then it's probably best to go for this one
			    // since it's most likely compatible with the monitor
			    if ((current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel()) &&
	                        (current.getFrequency() == Display.getDesktopDisplayMode().getFrequency())) {
	                            targetDisplayMode = current;
	                            break;
	                    }
	                }
	            }
	        } else {
	            targetDisplayMode = new DisplayMode(width,height);
	        }

	        if (targetDisplayMode == null) {
	            System.out.println("Failed to find value mode: "+width+"x"+height+" fs="+fullscreen);
	            return;
	        }

	        Display.setDisplayMode(targetDisplayMode);
	        Display.setFullscreen(fullscreen);
				
	    } catch (LWJGLException e) {
	        System.out.println("Unable to setup mode "+width+"x"+height+" fullscreen="+fullscreen + e);
	    }
	}

	public boolean isFullscreen() {
		return fullscreen;
	}
}
